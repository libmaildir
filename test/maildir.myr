use fileutil
use std
use sys
use testr

use maildir

var testroot : byte[:]

const main = {
        testroot = [][:]
        match std.mkdtemp("test_root_for_libmaildir", 0o700)
        | `std.Ok p:
                testroot = p
        | `std.Err e:
                testr.run([
                        [.name="setup", .fn = {c : testr.ctx#
                                testr.fail(c, "mktemp failed: {}", e)
                        }]
                ][:])
                std.exit(0)
        ;;

        if !std.chdir(testroot)
                testr.run([
                        [.name="setup", .fn = {c : testr.ctx#
                                testr.fail(c, "chdir({}) failed", testroot)
                        }]
                ][:])
                std.exit(0)
        ;;

        testr.run([
                [.name="changeinfo01", .fn = changeinfo01],

                [.name="cleanfs01", .fn = cleanfs01],

                [.name="info01", .fn = info01],

                [.name="infofmt01", .fn = infofmt01],

                [.name="maildirs01", .fn = maildirs01],
                [.name="maildirs02", .fn = maildirs02],
                [.name="maildirs03", .fn = maildirs03],

                [.name="messages01", .fn = messages01],
                [.name="messages02", .fn = messages02],

                [.name="movemessage01", .fn = movemessage01],
        ][:])

        /* We don't remove testroot. It shouldn't be a concern */
        std.slfree(testroot)
        testroot = [][:]
}

/* Utility functions */
const settimes = {c : testr.ctx#, p : byte[:][:], t : uint64
        var tv :  sys.timeval = [ .sec = t ]
        var tvs : sys.timeval[2] = [ tv, tv ]
        for q : p
                /* TODO: once syscalls are cleaned up, this needs to change */
                match sys.utimes(sys.cstring(q), &tvs[0])
                | 0:
                | e:
                        testr.fail(c, "cannot set time of {}: {}", q, e)
                        -> false
                ;;
        ;;

        -> true
}

const tree = {p : byte[:][:] -> std.result(byte[:][:], byte[:])
        var r : byte[:][:] = std.slalloc(0)

        for q : p
                for f : fileutil.bywalk(q)
                        std.slpush(&r, std.sldup(f))
                ;;
        ;;

        -> `std.Ok std.sort(r, std.strcmp)
}

const mkpaths = {c : testr.ctx#, p : byte[:][:] -> bool
        for q : p
                match std.mkpath(q)
                | std.Enone:
                | e:
                        testr.fail(c, "could not mkpath {}: {}", q, e)
                        -> false
                ;;
        ;;

        -> true
}

const mkfiles = {c : testr.ctx#, f : byte[:][:] -> bool
        var s : std.strbuf# = std.mksb()
        var p : byte[:]
        var j : int

        for ff : f
                std.sbtrim(s, 0)
                std.sbfmt(s, "{}", ff)
                p = std.sbpeek(s)
                for j = p.len - 1; j > 0; j--
                        if p[j] == ('/' : byte)
                                std.sbtrim(s, (j : std.size))
                                match std.mkpath(std.sbpeek(s))
                                | std.Enone:
                                | e:
                                        testr.fail(c, "could not mkpath {}: {}", std.sbpeek(s), e)
                                        std.sbfree(s)
                                        -> false
                                ;;
                                break
                        ;;
                ;;
        ;;

        std.sbfree(s)

        for ff : f
                match std.openmode(ff, std.Ocreat | std.Oappend, 0o700)
                | `std.Ok h: std.close(h)
                | `std.Err e:
                        testr.fail(c, "could not open {}: {}", ff, e)
                        -> false
                ;;
        ;;

        -> true
}

const beat = {c : testr.ctx#, d : byte[:] -> bool
        if !std.chdir(testroot)
                testr.fail(c, "could not chdir to {}", testroot)
                 -> false
        ;;

        match std.mkpath(d)
        | std.Enone:
        | e:
                testr.fail(c, "could not mkpath {}: {}", d, e)
                 -> false
        ;;

        if !std.chdir(d)
                testr.fail(c, "could not chdir to {}", d)
                 -> false
        ;;

        -> true
}

const strseq = {a : byte[:][:], b : byte[:][:] -> bool
        var i : uint = 0

        if a.len != b.len
                -> false
        ;;

        while i < a.len
                match std.strcmp(a[i], b[i])
                | `std.Equal:
                | _: -> false
                ;;

                i++
        ;;

        -> true
}


/* Actual tests */
const changeinfo01 = {c : testr.ctx#
        var expected : byte[:] = [][:]
        var expected2 : byte[:][:] = [][:]
        var i : maildir.info = [ .passed = true, .seen = true ]

        if !beat(c, "changeinfo01")
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp" ][:])
                -> void
        ;;

        if !mkfiles(c, [ "a/cur/abcdefg.123:2,S" ][:])
                -> void
        ;;

        expected = "a/cur/abcdefg.123:2,PS"

        match maildir.changeinfo("a/cur/abcdefg.123:2,S", i)
        | `std.Ok actual:
                testr.check(c, std.eq(actual, expected), \
                            "expected “{}”, got “{}”", expected, actual)
        | `std.Err e:
                testr.fail(c, "{}", e)
                -> void
        ;;

        expected = "link(\"a/cur/abcdefg.123:2,S\", \"a/cur/abcdefg.123:2,PS\"): -2"

        match maildir.changeinfo("a/cur/abcdefg.123:2,S", i)
        | `std.Ok actual:
                testr.fail(c, "got “{}”, expected error")
                -> void
        | `std.Err actual:
                testr.check(c, std.eq(actual, expected), \
                            "expected “{}”, got “{}”", expected, actual)
        ;;

        expected2 = [ "a/cur/abcdefg.123:2,PS" ][:]

        match tree([ "a" ][:])
        | `std.Ok actual:
                testr.check(c, strseq(expected2, actual), \
                            "expected “{}”, got “{}”", expected2, actual)
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;
}

const cleanfs01 = {c : testr.ctx#
        var s : std.strbuf# = std.mksb()
        var expected : byte[:][:] = [][:]
        var ago35 : uint64 = ((std.now() / std.Sec) : uint64) - (35 * 60 * 60)
        var ago37 : uint64 = ((std.now() / std.Sec) : uint64) - (37 * 60 * 60)

        if !beat(c, "cleanfs01")
                std.sbfree(s)
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp",
                         "b/cur", "b/new", "b/tmp",
                         "c/cur", "c/new", "c/tmp" ][:])
                std.sbfree(s)
                -> void
        ;;

        expected = [ "a/cur/101:2,T",
                     "a/cur/102:2,FS",
                     "a/new/103:2,",
                     "a/new/104:2,R",
                     "a/tmp/105",
                     "a/tmp/106",
                     "b/cur/107:2,T",
                     "b/cur/108:2,S",
                     "b/new/109:2,",
                     "b/new/110:2,R",
                     "b/tmp/111",
                     "b/tmp/112",
                     "c/cur/113:2,T",
                     "c/cur/114:2,FP",
                     "c/new/115:2,",
                     "c/new/116:2,R",
                     "c/tmp/117",
                     "c/tmp/118",
                    ][:]

        if !mkfiles(c, expected)
                -> void
        ;;

        match tree(["a", "b", "c"][:])
        | `std.Ok actual:
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

        if !settimes(c, [ "a/cur/101:2,T",
                          "a/new/103:2,",
                          "a/tmp/105",
                          "b/tmp/112",
                          "c/tmp/117" ][:], ago37)
                -> void
        ;;

        if !settimes(c, [ "a/tmp/106",
                          "b/tmp/111",
                          "c/tmp/118" ][:], ago35)
                -> void
        ;;

        expected = [ "a/cur/102:2,FS",
                     "a/new/103:2,",
                     "a/new/104:2,R",
                     "a/tmp/106",
                     "b/cur/108:2,S",
                     "b/new/109:2,",
                     "b/new/110:2,R",
                     "b/tmp/111",
                     "c/cur/113:2,T",
                     "c/cur/114:2,FP",
                     "c/new/115:2,",
                     "c/new/116:2,R",
                     "c/tmp/117",
                     "c/tmp/118",
                    ][:]
        match maildir.cleanfs(["a", "b"][:])
        | `std.Ok _:
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

        match tree(["a", "b", "c"][:])
        | `std.Ok actual:
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

}

const info01 = {c : testr.ctx#
        var m : byte[:][:] = [][:]
        var i : maildir.info = [ .trashed = false ]
        var as : std.strbuf# = std.mksb()
        var es : std.strbuf# = std.mksb()
        var asp : byte[:] = ""
        var esp : byte[:] = ""
        var expected : (byte[:], maildir.info)[:]
        var checked : bool = false

        if !beat(c, "info01")
                std.sbfree(as)
                std.sbfree(es)
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp" ][:])
                std.sbfree(as)
                std.sbfree(es)
                -> void
        ;;

        if !mkfiles(c, [ "a/new/somethinguniq⁷_##$$Foo",
                         "a/new/765786.288.257968732.6795821",
                         "a/cur/abc.def:2,P",
                         "a/cur/zzzz:2",
                         "a/cur/zzzy:2,",
                         "a/cur/abc.d∞g:2,FRS",
                         "a/cur/abc:Fdeg:2,DPT",
                        ][:])
                std.sbfree(as)
                std.sbfree(es)
                -> void
        ;;

        expected = [
                ("a/new/somethinguniq⁷_##$$Foo", \
                 [ .passed = false ]),
                ("a/new/765786.288.257968732.6795821", \
                 [ .passed = false ]),
                ("a/cur/abc.def:2,P", \
                 [ .passed = true ]),
                ("a/cur/zzzz:2", \
                 [ .passed = false ]),
                ("a/cur/zzzy:2,", \
                 [ .passed = false ]),
                ("a/cur/abc.d∞g:2,FRS", \
                 [ .flagged = true, .replied = true, .seen = true ]),
                ("a/cur/abc:Fdeg:2,DPT", \
                 [ .draft = true, .passed = true, .trashed = true ]),
        ][:]

        match maildir.messages("a")
        | `std.Ok mm:
                m = mm
                if m.len != expected.len
                        testr.fail(c, "some expected messages missing")
                        std.sbfree(as)
                        std.sbfree(es)
                        -> void
                ;;
        | `std.Err e:
                testr.fail(c, "can't list messages of a: {}", e)
                std.sbfree(as)
                std.sbfree(es)
                -> void
        ;;

        for p : m
                i = maildir.info(p)
                std.sbtrim(as, 0)
                std.sbfmt(as, "{}", i)
                asp = std.sbpeek(as)
                checked = false

                for (ep, ei) : expected
                        if std.eq(p, ep)
                                std.sbtrim(es, 0)
                                std.sbfmt(es, "{}", ei)
                                esp = std.sbpeek(es)
                                checked = true
                                testr.check(c, std.eq(asp, esp), \
                                            "for {}, expected “{}”, got “{}”", \
                                            p, esp, asp)
                        ;;
                ;;

                if !checked
                        testr.fail(c, "unexpected message {}", p)
                ;;
        ;;

        std.sbfree(as)
        std.sbfree(es)
}

const infofmt01 = {c : testr.ctx#
        var s : std.strbuf# = std.mksb()
        var i : maildir.info = [ .passed = true ]
        var actual : byte[:] = [][:]
        var expected : byte[:] = [][:]

        std.sbtrim(s, 0)
        std.sbfmt(s, "{}", i)
        expected = "P"
        actual = std.sbpeek(s)
        testr.check(c, std.eq(expected, actual), "expected “{}”, got “{}”", \
                    expected, actual)

        i = [ .flagged = true, .replied = true, .trashed = true ]
        std.sbtrim(s, 0)
        std.sbfmt(s, "{}", i)
        expected = "FRT"
        actual = std.sbpeek(s)
        testr.check(c, std.eq(expected, actual), "expected “{}”, got “{}”", \
                    expected, actual)

        i = [ .flagged = false ]
        std.sbtrim(s, 0)
        std.sbfmt(s, "{}", i)
        expected = ""
        actual = std.sbpeek(s)
        testr.check(c, std.eq(expected, actual), "expected “{}”, got “{}”", \
                    expected, actual)
}

const maildirs01 = {c : testr.ctx#
        var expected : byte[:][:] = [][:]
        var actual : byte[:][:] = [][:]
        if !beat(c, "maildirs01")
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp",
                         "b/cur", "b/new", "b/tmq",
                         "c/cur", "c/new", "c/tmq", "c/other",
                         "d",
                         "cur/cur", "cur/new", "cur/tmp",
                    ][:])
                -> void
        ;;

        expected = [ "a", "cur" ][:]
        match maildir.maildirs(".")
        | `std.Ok a:
                actual = a
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
                for aa : a
                        std.slfree(aa)
                ;;
                std.slfree(a)
        | `std.Err e:
                testr.fail(c, "can't examine .: {}", e)
        ;;
}

const maildirs02 = {c : testr.ctx#
        var expected : byte[:][:] = [][:]
        var actual : byte[:][:] = [][:]

        if !beat(c, "maildirs02/a/long/way/down")
                -> void
        ;;

        if !mkpaths(c, [ "d/cur", "d/new", "d/tmp",
                         "a/cur", "a/new", "a/tmq",
                         "c/cur", "c/new", "c/tmq", "c/other",
                         "d/extraneous",
                         "cur/cur", "cur/new", "cur/tmp",
                    ][:])
                -> void
        ;;

        if !beat(c, "maildirs02")
                -> void
        ;;

        expected = [ "a/long/way/down/cur", "a/long/way/down/d" ][:]
        match maildir.maildirs("a/long/way/down")
        | `std.Ok a:
                actual = a
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
                for aa : a
                        std.slfree(aa)
                ;;
                std.slfree(a)
        | `std.Err e:
                testr.fail(c, "can't examine a/long/way/down: {}", e)
        ;;
}

const maildirs03 = {c : testr.ctx#
        var expected : byte[:] = [][:]

        if !beat(c, "maildirs03")
                -> void
        ;;

        match maildir.maildirs("/dev/some/place/that/really/shouldn't/exist")
        | `std.Ok _:
                testr.fail(c, "got maildirs where none should be")
        | `std.Err e:
                /* TODO: should we really be checking this pedantically? */
                expected = "cannot open /dev/some/place/that/really/shouldn't/exist: couldn't open directory"
                testr.check(c, std.eq(e, expected), \
                            "expected “{}”, got “{}”", \
                            expected, e)
        ;;
}

const messages01 = {c : testr.ctx#
        var expected : byte[:][:] = [][:]
        var actual : byte[:][:] = [][:]
        if !beat(c, "messages01")
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp", "a/.qmail",
                         "a/new/foo", "a/cur/baz/quux",
                         "a/new/.ignorethis", "a/cur/......thistoo"
                    ][:])
                -> void
        ;;

        expected = std.sort([ "a/new/foo", "a/cur/baz" ][:], std.strcmp)
        match maildir.messages("a")
        | `std.Ok a:
                testr.check(c, strseq(expected, a), \
                            "expected “{}”, got “{}”", expected, a)
                for aa : a
                        std.slfree(aa)
                ;;
                std.slfree(a)
        | `std.Err e:
                testr.fail(c, "can't list messages of a: {}", e)
        ;;
}

const messages02 = {c : testr.ctx#
        var expected : byte[:] = [][:]
        if !beat(c, "messages02")
                -> void
        ;;

        if !mkpaths(c, [ "b/cur", "b/new", "b/NOTtmp",
                         "b/new/foo", "b/cur/baz"
                    ][:])
                -> void
        ;;

        match maildir.messages("b")
        | `std.Ok a:
                testr.fail(c, "got messages where none should be")
        | `std.Err e:
                expected = "b is not a maildir"
                testr.check(c, std.eq(e, expected), \
                            "expected “{}”, got “{}”", \
                            expected, e)
        ;;
}

const movemessage01 = {c : testr.ctx#
        var s : std.strbuf# = std.mksb()
        var expected : byte[:][:] = [][:]

        if !beat(c, "movemessage01")
                std.sbfree(s)
                -> void
        ;;

        if !mkpaths(c, [ "a/cur", "a/new", "a/tmp",
                         "b/cur", "b/new", "b/tmp" ][:])
                std.sbfree(s)
                -> void
        ;;

        if !mkfiles(c, [ "a/cur/101:2,FT" ][:])
                -> void
        ;;

        expected = [ "a/cur/101:2,FT" ][:]

        match tree(["a", "b"][:])
        | `std.Ok actual:
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

        match maildir.movemessage("a/cur/101:2,FT", "b")
        | `std.Ok _:
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

        expected = [ "b/cur/101:2,FT" ][:]

        match tree(["a", "b"][:])
        | `std.Ok actual:
                testr.check(c, strseq(expected, actual), \
                            "expected “{}”, got “{}”", expected, actual)
        | `std.Err e:
                testr.fail(c, e)
                -> void
        ;;

}
